#! /bin/bash

# Get the command line options
while getopts "b:h?" opt; do
	case "$opt" in
	b) BRANCH=$OPTARG ;;
	h) HELP_OPT=true;;
	?) HELP_OPT=true;;
	esac
done
# Render the help text
if [ -n "$HELP_OPT" ]; then
	echo "Usage of: $0"
	cat << EndOfMessage

Options
-------
 -b         the branch name to checkout, if omitted then the feature branch should be checked out
 -h or -?   display this help message

EndOfMessage
  exit -1
fi

#---------------------------------------------------------------------------------------------------
# Check the parameters

echo "Check the parameters"

# If a branch name has been provided then checkout the branch
if [[ ! -z $BRANCH ]]; then
	echo "Checkout the feature branch ${BRANCH}"
	git checkout $BRANCH

	# If the previous command failed then exit with an error code
	if [[ $? -ne 0 ]]; then
		echo "Error: Checkout failed"
		exit 1
	fi

	git pull
fi

# --------------------------------------------------------------------------------------------------
# Check the branch

echo
echo "Checking the branch"

# Get the name of the current branch
FEATURE_BRANCH=$(git rev-parse --abbrev-ref HEAD)
echo "Current branch is ${FEATURE_BRANCH}"

# Check the current branch is a feature branch
if [[ ${FEATURE_BRANCH} != feature/* ]]; then
    echo "Not on a feature branch, current branch is: ${FEATURE_BRANCH} "
    exit 1
fi

# Check for local changes
if [[ `git status --porcelain` ]]; then
    echo "You have untracked/uncommitted changes, do something about this first"
    exit 1
fi

echo "Work out what trhe tag should be?"

BASE_VERSION=$(cat package.json | jq '.version' | cut -d "\"" -f 2)
echo "The base version is ${BASE_VERSION}"

FEATURE_VERSION=$(echo $FEATURE_BRANCH | cut -d "/" -f 2 | sed -e "s/[^[:alpha:].-]/-/g")

echo "The feature version is $FEATURE_VERSION"

GIT_SHORT_HASH=$(git log -n 1 --oneline | awk '{print $1}')

echo "Git short hash $GIT_SHORT_HASH"


EXP_TAG="$BASE_VERSION-$FEATURE_VERSION-$GIT_SHORT_HASH"


echo "The exp tag is $EXP_TAG"

echo "Push any outstanding changes"

git push origin $FEATURE_BRANCH

echo "Add the tag"

git tag $EXP_TAG

git push origin $EXP_TAG

#! /bin/bash

#
# This script uses Semantic Versioning 2.0.0
# https://semver.org/

S_MAJOR="major"
S_MINOR="minor"
S_PATCH="patch"

# Get the command line options
while getopts "p:b:" opt; do
	case "$opt" in
	p) SEMVER_UPDATE_PART=$OPTARG;;
	b) BRANCH=$OPTARG;;
	esac
done

# Check is current directory
if [ ! -f "build/update_version.sh" ]
then
    echo "Run this from the directory it project root"
    exit 1
fi


# Check for local changes
if [[ `git status --porcelain` ]]; then
    echo "You have untracked/uncommitted changes, do something about this first"
    exit 1
fi

# Checkout the branch
echo "Branch: $BRANCH"
if [[ ! -z $BRANCH ]]
then
	echo "Checkout the branch"
	git checkout $BRANCH
    if [[ $? -ne 0 ]]; then
        echo "Error: Checkout failed"
        exit 1
    fi

	git pull
fi

FEATURE_BRANCH=$(git rev-parse --abbrev-ref HEAD)

# --------------------------------------------------------------------------------------------------
# Update master

echo
echo "Merge the feature branch ($FEATURE_BRANCH) back to master"
echo "Make sure we have the latest master"

git checkout master
git pull --rebase


# check_semver_update
if [[ -z $SEMVER_UPDATE_PART ]]
then
    echo "set the part to default ($S_MINOR)"
    SEMVER_UPDATE_PART=$S_MINOR
fi

if [[ $SEMVER_UPDATE_PART == $S_MAJOR ]] || [[ $SEMVER_UPDATE_PART == $S_MINOR ]] || [[ $SEMVER_UPDATE_PART == $S_PATCH ]]
then
    echo "Bumping by a $SEMVER_UPDATE_PART version"
else
    echo "Cant find the part '$SEMVER_UPDATE_PART'"
    exit 1
fi

echo "Merge the branch to master"
git merge $FEATURE_BRANCH

echo "Bump the npm version"
npm version $SEMVER_UPDATE_PART

echo "Push the changes"

git push
git push --tags

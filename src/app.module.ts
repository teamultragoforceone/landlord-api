import {HttpModule, MiddlewareConsumer, Module, NestModule, RequestMethod} from '@nestjs/common';
import {AppController} from './controllers/app.controller';
import {AppService} from './app.service';
import {AuthController} from './controllers/auth/auth.controller';
import {AuthService} from './service/auth/auth.service';
import {UsersService} from './service/users/users.service';
import {PassportModule} from '@nestjs/passport';
import {HttpStrategy} from './service/auth/http.strategy';
import {ConfigService} from './config/config.service';
import {HealthController} from './controllers/health/health.controller';
import {CorsMiddleware} from './http/cors.middleware';
import {EndpointsService} from './service/endpoints/endpoints.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import {UserEntity} from './service/users/user.entity';
import { EstateController } from './controllers/estate/estate.controller';
import { EstateService } from './service/estate/estate.service';
import {EstateEntity} from './service/estate/estate.entity';
@Module({
  imports: [
    PassportModule.register({defaultStrategy: 'bearer'}),
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),

    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'defalt',
      password: 'JP)(2407d',
      database: 'wikixo',
      entities: [__dirname + '/**/*.entity.ts'],
      synchronize: false,
      insecureAuth : true,
    }),
    TypeOrmModule.forFeature(
      [
        UserEntity,
        EstateEntity,
      ],
    ),
  ],
  controllers: [
    AppController,
    AuthController,
    HealthController,
    EstateController,
  ],
  providers: [
    AppService,
    AuthService,
    UsersService,
    HttpStrategy,
    ConfigService,
    {
      provide: ConfigService,
      /*useValue: new ConfigService(`${process.env.NODE_ENV}.env`),*/
      useValue: new ConfigService(),
    },
    EndpointsService,
    EstateService],
  exports: [ConfigService],
})
export class AppModule implements NestModule {

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(CorsMiddleware)
      .forRoutes({path: '*', method: RequestMethod.ALL});
    return undefined;
  }

}

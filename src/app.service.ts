import {Injectable, Res} from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(@Res() res): string {
    return 'Hello World! !';
  }
}

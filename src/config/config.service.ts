import {Injectable} from '@nestjs/common';
import {Config} from './config';

const dotenv = require('dotenv').config();

@Injectable()
export class ConfigService {
  private readonly envConfig: { [key: string]: string };
  public readonly config: Config;

  constructor() {
    // @ts-ignore
    this.config = {
      biHistoryRoot: this.trimSlash(process.env.BI_HIST_ADDR) || 'http://mongo-event-service.event-streaming',
      biAnalyticsRoot: this.trimSlash(process.env.BI_ANALYTICS_ADDR) || 'http://event-gateway.wikixo.com',
      biGameDataAddress: this.trimSlash(process.env.BI_GAME_DATA_ADDR) || 'http://game-analytics-service.event-streaming',
      hubRoot: this.trimSlash(process.env.HUB_ADDR) || 'http://hub-stable.ws.u2g',
      tierRoot: this.trimSlash(process.env.TIER_ADDR) || 'http://tier-dev.ws.u2g',
      websiteSemVer: process.env.npm_package_version || null, // IMPORTANT -> should be lower-case
      tierName: process.env.TIER_NAME || 'dev',
      cloudSecurityAccountKey: process.env.CLOUD_SECURITY_ACCOUNT_KEY || 'projectzero-199415-2621174d42a4.json',
      cloudSecurityProjectId: process.env.CLOUD_SECURITY_PROJECT_ID || 'website',
      cloudStorageBucketName: process.env.CLOUD_STORAGE_BUCKET_NAME || 'u2g-projectzero-storage-dev',
      // cloudStorageMaxFileSize: process.env.CLOUD_STORAGE_MAX_FILE_SIZE || 1024 * 1024,
      // cloudStorageImageUrl: this.trimSlash(process.env.CLOUD_STORAGE_IMAGE_URL) || 'https://testing.content.wikixo.com/website/game/',
      discordRedirect: process.env.DISCORD_REDIRECT || 'http://localhost:4200/discordauth',
    };

    // this.envConfig = dotenv.parse(fs.readFileSync(filePath));
  }

  get(key: string): string {
    return this.envConfig[key];
  }

  private trimSlash(value: string): string {
    return value ? value.replace(/\/$/, '') : null;
  }
}

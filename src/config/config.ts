export class Config {
  hubRoot: string = '';
  tierRoot: string = '';
  websiteSemVer: string = '';
  tierName: string = '';
  biHistoryRoot: string = '';
  biAnalyticsRoot: string = '';
  biGameDataAddress: string = '';
  cloudSecurityAccountKey: string = '';
  cloudSecurityProjectId: string = '';
  cloudStorageBucketName: string = '';
  // cloudStorageMaxFileSize: any;
  // cloudStorageImageUrl: string = '';
  discordRedirect = '';
}

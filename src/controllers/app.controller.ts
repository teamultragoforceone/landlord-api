import {Controller, Get, Res} from '@nestjs/common';
import {util} from 'protobufjs';
import {ConfigService} from '../config/config.service';
import fs = util.fs;

@Controller()
export class AppController {
  constructor(
    protected readonly configService: ConfigService,
  ) {
  }

  @Get()
  getHello(@Res() res): void {
    res.redirect('/swagger');
  }

}

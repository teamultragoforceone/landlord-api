import {Test, TestingModule} from '@nestjs/testing';
import {AuthController} from './auth.controller';
import {ConfigService} from '../../config/config.service';
import {UsersService} from '../../service/users/users.service';

describe('Auth Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        ConfigService,
        UsersService,
      ],
    }).compile();
  });
  it('should be defined', () => {
    const controller: AuthController = module.get<AuthController>(AuthController);
    expect(controller).toBeDefined();
  });
});

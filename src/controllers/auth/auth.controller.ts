import {Body, Controller, Get, Param, Post, Render, Req, Res, UseGuards} from '@nestjs/common';
import {UsersService} from '../../service/users/users.service';
import {ConfigService} from '../../config/config.service';
import {AuthGuard} from '@nestjs/passport';
import {CreateUserDTO, UserLoginDTO} from '../../dto/UserDTO';
import {UserEntity} from '../../service/users/user.entity';

@Controller('auth')
export class AuthController {

  constructor(
    private usersService: UsersService,
    private configService: ConfigService,
  ) {
  }

  @Post('login')
  async postLogin(@Body() userLoginDTO: UserLoginDTO): Promise<{ token: string }> {
    console.log('++ postLogin()');
    return this.usersService.userAuth(userLoginDTO.user, userLoginDTO.password).then((response) => {
      return {token: response.token};
    });

  }

  @Post('register')
  async postRegister(@Body() createUserDTO: CreateUserDTO): Promise<{ token: string }> {
    return this.usersService.userRegister(createUserDTO as UserEntity).then((userEntity) => {
      return {token: userEntity.token};
    });
  }

  @Get('logout')
  @UseGuards(AuthGuard('bearer'))
  async postLogout(): Promise<boolean> {
    return this.usersService.newUserToken();
  }

  @Get('eula')
  getEula(@Res() res, @Req() req): void {

  }

  @Post('acceptEula')
  getProcessEula(@Res() res, @Req() req, @Body('eula') eula, @Body('token') token): void {

  }

  @Post('validateKey')
  postValidateKey(@Res() res, @Req() req, @Body('key') accessKey, @Body('token') token): void {

  }

}

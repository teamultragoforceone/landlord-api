import {Body, Controller, Get, Post} from '@nestjs/common';
import {EstateService} from '../../service/estate/estate.service';
import {CreateUserDTO} from '../../dto/UserDTO';
import {CreateEstateDTO} from '../../dto/EstateDTO';
import {UsersService} from '../../service/users/users.service';

@Controller('estate')
export class EstateController {

  constructor(
    private estateService: EstateService,
    private userService: UsersService,
  ) {
  }

  @Post('new')
  async postRegister(@Body() createEstateDTO: CreateEstateDTO): Promise<boolean> {
    return this.estateService.new(this.userService.getCurrentUser().id, createEstateDTO.name);
  }
  @Get('')
  get(): boolean {
    return true;
  }
  @Get(':id')
  getById(): boolean {
    return  false;
   // return this.estateService.new(this.userService.getCurrentUser().id, createEstateDTO.name);

  }

/*
  async findOneByUser(userNaem: string) {
    return await this.userRepository.findOne({user: userNaem});
  }
*/

}

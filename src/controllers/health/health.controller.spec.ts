import {Test, TestingModule} from '@nestjs/testing';
import {HealthController} from './health.controller';
import {UsersService} from '../../service/users/users.service';
import {ConfigService} from '../../config/config.service';
import {HttpModule} from '@nestjs/common';
import {AppService} from '../../app.service';
import {AuthService} from '../../service/auth/auth.service';
import {HttpStrategy} from '../../service/auth/http.strategy';
import {PassportModule} from '@nestjs/passport';

describe('Health Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        PassportModule.register({defaultStrategy: 'bearer'}),
        HttpModule.register({
          timeout: 5000,
          maxRedirects: 5,
        }),
      ],
      controllers: [HealthController],
      providers: [
        AppService,
        AuthService,
        UsersService,
        HttpStrategy,
        ConfigService,
        {
          provide: ConfigService,
          useValue: new ConfigService(),
        },
      ],
    }).compile();
  });
  it('should be defined', () => {
    const controller: HealthController = module.get<HealthController>(HealthController);
    expect(controller).toBeDefined();
  });

  it('say is alive', () => {
    const controller: HealthController = module.get<HealthController>(HealthController);
    const ok = {
      status: 'UP',
    };
    expect(controller.health()).toMatchObject(ok);
  });

  it('check info', () => {
    const controller: HealthController = module.get<HealthController>(HealthController);
    const ok = {
      biAnalyticsRoot: expect.stringContaining('http'),
      biHistoryRoot: expect.stringContaining('http'),
      hubRoot: expect.stringContaining('http'),
      tierRoot: expect.stringContaining('http'),
      build: {
        version: expect.stringContaining('.'),
      },
    };
    expect(controller.info()).toMatchObject(ok);
  });

});

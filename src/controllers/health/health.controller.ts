import {Controller, Get, UseGuards} from '@nestjs/common';
import {ConfigService} from '../../config/config.service';
import {AuthGuard} from '@nestjs/passport';

@Controller('')
export class HealthController {

  constructor(protected configService: ConfigService) {
  }

  @Get('/health')
  health() {
    return {
      status: 'UP',
    };
  }

  @Get('/info')
  info() {
    return {
      hubRoot: this.configService.config.hubRoot,
      tierRoot: this.configService.config.tierRoot,
      biHistoryRoot: this.configService.config.biHistoryRoot,
      biAnalyticsRoot: this.configService.config.biAnalyticsRoot,
      build: {
        version: this.configService.config.websiteSemVer,
      },
    };
  }

  @Get('/health/auth')
  @UseGuards(AuthGuard('bearer'))
  healthAuth() {
    return {
      status: 'UP',
      version: this.configService.config.websiteSemVer,
    };
  }
}

import { IsEmail, IsNotEmpty } from 'class-validator';
export class CreateEstateDTO {
  @IsNotEmpty()
  public name: string;
}

export class CreateUserDTO {
  public user: string;
  public password: string;
}
export class SetUserNameDTO {
  user: string;
  password: string;
}
export class SetUserPasswordDTO {
  password: string;
}
export class UserLoginDTO {
  user: string;
  password: string;
}

import {HttpStatus, Injectable, MiddlewareFunction, NestMiddleware, RequestMethod} from '@nestjs/common';
import * as cors from 'cors';

/*
 See

 Nest cors example https://github.com/surmon-china/nodepress/blob/master/src/middlewares/cors.middleware.ts

 https://github.com/nestjs/nest/issues/165

 Cors package https://www.npmjs.com/package/cors
  */
@Injectable()
export class CorsMiddleware implements NestMiddleware {
  resolve(): MiddlewareFunction {
    return (request, response, next) => {

      const getMethod = method => RequestMethod[method];
      const origin = request.headers.origin || '';
      // const allowedOrigins = [...APP_CONFIG.CROSS_DOMAIN.allowedOrigins];
      const allowedOrigins = [];
      const allowedMethods = [RequestMethod.GET, RequestMethod.HEAD, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.POST, RequestMethod.DELETE];
      const allowedHeaders = ['Authorization', 'Origin', 'No-Cache', 'X-Requested-With', 'If-Modified-Since', 'Pragma', 'Last-Modified', 'Cache-Control', 'Expires', 'Content-Type', 'X-E4M-With'];

      // Allow Origin
      // if (!origin || allowedOrigins.includes(origin) || isDevMode) {
      //   //response.setHeader('Access-Control-Allow-Origin', origin || '*');
      // }

      response.setHeader('Access-Control-Allow-Origin', '*');

      // Headers
      response.header('Access-Control-Allow-Headers', allowedHeaders.join(','));
      response.header('Access-Control-Allow-Methods', allowedMethods.map(getMethod).join(','));
      response.header('Access-Control-Max-Age', '1728000');
      response.header('Content-Type', 'application/json; charset=utf-8');
      // response.header('X-Powered-By', `Nodepress ${APP_CONFIG.INFO.version}`);

      // OPTIONS Request
      if (request.method === getMethod(RequestMethod.OPTIONS)) {
        return response.sendStatus(HttpStatus.NO_CONTENT);
      } else {
        return next();
      }
    };
  }
}

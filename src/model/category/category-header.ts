export class CategoryHeader {
  pageSize: number;
  pageStart: number;
  total: number;
}

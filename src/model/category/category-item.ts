export class CategoryItem {
  gameId: string;
  name: string;
  description: string;
  ownerId: string;
  created: any;
  updated: any;
  ownerName: string;
  publiclyEditable: boolean;
  tags: any[];
  hidden: boolean;
  systemGame: boolean;
  copyable: boolean;
  isOwner: boolean;
  linkId: string;
  hasLink: boolean;
}

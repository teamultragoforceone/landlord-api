import {CategoryItem} from './category-item';
import {CategoryHeader} from './category-header';

export class Category {
  list: {
    header: CategoryHeader[];
    items: CategoryItem[];
  };

  name: string;
}

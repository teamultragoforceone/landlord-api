import {Test, TestingModule} from '@nestjs/testing';
import {AuthService} from './auth.service';
import {UsersService} from '../users/users.service';
import {AppService} from '../../app.service';
import {HttpStrategy} from './http.strategy';
import {ConfigService} from '../../config/config.service';

describe('AuthService', () => {
  let service: AuthService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AppService,
        AuthService,
        UsersService,
        HttpStrategy,
        ConfigService,
        {
          provide: ConfigService,
          useValue: new ConfigService(),
        },
      ],
    }).compile();
    service = module.get<AuthService>(AuthService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

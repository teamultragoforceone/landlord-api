import {Injectable} from '@nestjs/common';
import {UsersService} from '../users/users.service';
import {UserEntity} from '../users/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
  ) {
  }

  async validateUser(token: string): Promise<UserEntity> {
    return await this.usersService.userAuthByToken(token);
  }
}

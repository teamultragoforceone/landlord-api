import {HttpService, Injectable} from '@nestjs/common';
import {EndpointRequest} from './endpoints';
import {UsersService} from '../users/users.service';
import {ConfigService} from '../../config/config.service';
import {Backend} from './backend';

@Injectable()
export class EndpointsService {

  constructor(private readonly usersService: UsersService,
              private readonly configService: ConfigService,
              private readonly httpService: HttpService) {
  }

  public post(res, req, request: EndpointRequest) {
    console.log(`++ post(${JSON.stringify(request)})`);
    const url = `${this.getRoot(request.backend)}/${request.endpoint}`;
    const token = (this.usersService.getUserToken() ? this.usersService.getUserToken() : 'None');
    console.log(url + ' ' + token);
    this.httpService.post(url,
      {},
      {headers: {'X-Auth-Token': token}})
      .subscribe((r) => {
          console.log('-- post(...) ' + JSON.stringify(r.data));
          res.status(200).send(r.data);
        },
        (e) => {
          console.log('-- post(...) ' + e);
          res.status(200).send({});
        });
  }

  private getRoot(backend: Backend) {
    if (backend === Backend.hub) {
      return this.configService.config.hubRoot;
    } else {
      return this.configService.config.tierRoot;
    }
  }

}

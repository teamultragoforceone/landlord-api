import {Backend} from './backend';

export class EndpointRequest {

  backend: Backend;
  endpoint: string;

}

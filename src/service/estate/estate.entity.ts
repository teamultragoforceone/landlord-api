import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

@Entity({name: 'estate'})
export class EstateEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({length: 500, nullable: false})
  name: string;
  @Column({length: 500})
  estateAdmin: string;
}

import {Injectable} from '@nestjs/common';
import {ConfigService} from '../../config/config.service';
import {UserEntity} from '../users/user.entity';
import {Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {EstateEntity} from './estate.entity';
@Injectable()
export class EstateService {
  constructor(
    private readonly configService: ConfigService,
    @InjectRepository(EstateEntity)
    private readonly estateRepository: Repository<EstateEntity>,
  ) {
  }
  getbyId() {
    return{};
  }

  getallFouUserId() {
    return [];
  }

  async new(userId, name) {
    let stateEntity = new EstateEntity();
    stateEntity.name = name;
    stateEntity.estateAdmin = userId;

    return await this.estateRepository.save(stateEntity).then((userResponce) => {
           return true;
    });
  }
}

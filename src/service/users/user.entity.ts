import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

@Entity({name: 'user'})
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({length: 500, nullable: true})
  name: string;
  @Column({length: 500, unique: true})
  user: string;
  @Column({length: 500})
  password: string;
  @Column({length: 500})
  token: string;
  @Column({default: 2})
  type: number;
}

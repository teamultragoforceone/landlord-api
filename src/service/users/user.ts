export class User {
  id: number;
  name: string;
  user: string;
  password: string;
  permissions: string[];
  token: string;
}

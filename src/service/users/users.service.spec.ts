import {Test, TestingModule} from '@nestjs/testing';
import {UsersService} from './users.service';
import {ConfigService} from '../../config/config.service';

describe('UsersService', () => {
  let service: UsersService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersService, ConfigService],
    }).compile();
    service = module.get<UsersService>(UsersService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('can log in', async () => {
//     const userAproved = {
//      /* accessLevel: expect.stringContaining('FULL'),*/
//       accountId: expect.stringContaining('b3fa3034-beaf-4f53-9324-a0d0e2386d5d'),
//       token: expect.stringContaining('-'),
// /*      expiry: expect.toBeTruthy(),*/
//     };
//     await service.userAuth('eulatest1', 'eulatest1').then((user) => {
//       expect(user).toMatchObject(userAproved);
//     });
  });
});

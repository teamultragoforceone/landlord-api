import {Injectable, UnauthorizedException} from '@nestjs/common';
import {ConfigService} from '../../config/config.service';
import {User} from './user';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {UserEntity} from './user.entity';
import * as bcrypt from 'bcrypt';
import * as uuid from 'uuid/v4';

@Injectable()
export class UsersService {
  private saltRounds = 10;

  protected userToken: string;
  protected currentUser: UserEntity;

  constructor(
    private readonly configService: ConfigService,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {
  }

  async findAll(): Promise<UserEntity[]> {
    return await this.userRepository.find();
  }

  async findOneByToken(userToken: string) {
    return await this.userRepository.findOne({token: userToken});
  }

  async findOneByUser(userNaem: string) {
    return await this.userRepository.findOne({user: userNaem});
  }

  getUserToken() {
    return this.userToken;
  }

  getCurrentUser() {
    return this.currentUser;
  }

  /*

   userHasPermission(permission): boolean {
      return (this.userToken.permissions.indexOf(permission) <= -1);
    }
  */

  setUserToken(userToken: string) {
    return this.userToken = userToken;
  }

  setCurrentUser(userEntity: UserEntity) {
    return this.currentUser = userEntity;
  }

  async userAuth(user, pass): Promise<UserEntity> {
    return this.findOneByUser(user).then((userEnt) => {
      return this.compareHash(pass, userEnt.password).then((match) => {
        if (match) {
          userEnt.password = undefined;
          return userEnt;
        }
        throw new UnauthorizedException(' pass word miss mathc');
      });
    });
  }

  async userAuthByToken(token: string): Promise<UserEntity> {
    return this.findOneByToken(token).then((userEnt) => {
      if (userEnt) {
        this.setCurrentUser(userEnt);
        this.setUserToken(userEnt.token);
        return userEnt;
      }
      throw new UnauthorizedException(' there was no user');
    }).catch((e) => {
      throw new UnauthorizedException(e);
    });
  }

  async userRegister(userEntity: UserEntity): Promise<UserEntity> {
    userEntity.password = await this.getHash(userEntity.password);
    userEntity.token = await uuid();
    return await this.userRepository.save(userEntity).then((userResponce) => {
      userResponce.password = undefined;
      return userResponce;
    });
  }

  async getHash(password: string | undefined): Promise<string> {
    return bcrypt.hash(password, this.saltRounds);
  }

  async compareHash(password: string | undefined, hash: string | undefined): Promise<boolean> {
    return bcrypt.compare(password, hash);
  }

  async newUserToken() {
    let userEntity = this.getCurrentUser();
    userEntity.token = await uuid();
    return await this.userRepository.save(userEntity).then((userResponce) => {
      return true;
    });
  }
}

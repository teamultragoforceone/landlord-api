import { Selector } from 'testcafe';
import config from './config';

fixture`Create section`
    .page`${config.baseUrl}/create`;

test('Create game - Mine / Recent / Public', async t => {
    if (await Selector('#login-internal').exists) {
        await t
            .click(Selector('#login-internal'))
    }
    await t
        .typeText(Selector('#inputEmail'), 'eulatest1')
        .typeText(Selector('#inputPassword'), 'eulatest1')
        .click(Selector('.login--submit'))
        .wait(2000)
        .expect(Selector('.swal2-error').exists).notOk('My games page loaded with no issues', {
            timeout: 3000
        })
        .expect(Selector('#card-inner-new').visible).ok('Create game card exists', {
            timeout: 3000
        })
        .expect(Selector('#card-inner-template').visible).ok('Existing game card exists', {
            timeout: 3000
        })
        .navigateTo(`${config.baseUrl}/create?type=recent`)
        .expect(Selector('.swal2-error').exists).notOk('Recent edit page loaded with no issues', {
            timeout: 3000
        })
        .navigateTo(`${config.baseUrl}/create?type=public`)
        .expect(Selector('.swal2-error').exists).notOk('Publicly editable page loaded with no issues', {
            timeout: 3000
        })
        .expect(Selector('#card-inner-template').visible).ok('Public game card exists', {
            timeout: 10000
        });
});
    
test('Create game - Tutorial / Starter', async t => {
    if (await Selector('#login-internal').exists) {
        await t
            .click(Selector('#login-internal'))
    }
    await t
        .typeText(Selector('#inputEmail'), 'eulatest1')
        .typeText(Selector('#inputPassword'), 'eulatest1')
        .click(Selector('.login--submit'))
        .click(Selector('#card-inner-new').find('button').withText('Create New'))
        .expect(Selector('#game-create-modal').visible).ok('Create modal appeared', {
            timeout: 2000
        })
        .click(Selector('span').withText('Starter'))
        .typeText(Selector('.game-create__name'), 'Starter game')
        .click(Selector('.card.card--create-template[data-template-id="Collectables"][data-template-name="Collection Game"][data-template-type="starter"]').find('.card-img-top.create-template__image[alt="Template image"]'))
        .click(Selector('button').withText('Create game'))
        .expect(Selector('.swal2-success').visible).ok('Starter game created successfully', {
            timeout: 10000
        })
});

test('Create game - Example', async t => {
    if (await Selector('#login-internal').exists) {
        await t
            .click(Selector('#login-internal'))
    }
    await t
        .typeText(Selector('#inputEmail'), 'eulatest1')
        .typeText(Selector('#inputPassword'), 'eulatest1')
        .click(Selector('.login--submit'))
        .click(Selector('#card-inner-new').find('button').withText('Create New'))
        .expect(Selector('#game-create-modal').visible).ok('Create modal appeared', {
            timeout: 2000
        })
        .click(Selector('span').withText('Example'))
        .typeText(Selector('.game-create__name'), 'Example game')
        .click(Selector('.card.card--create-template[data-template-id="7d1d248d-58d7-49e2-92e8-6b1705dee965"][data-template-name="Vertigo Skyline"][data-template-type="example"]').find('.card-img-top.create-template__image[alt="Template image"]'))
        .click(Selector('button').withText('Create game'))
        .expect(Selector('.swal2-success').visible).ok('Example game created successfully', {
            timeout: 10000
        });
});

test('Edit game - Name, description & image', async t => {
    if (await Selector('#login-internal').exists) {
        await t
            .click(Selector('#login-internal'))
    }
    await t
        .typeText(Selector('#inputEmail'), 'eulatest1')
        .typeText(Selector('#inputPassword'), 'eulatest1')
        .click(Selector('.login--submit'))
        .click(Selector('#card-inner-template').find('.play-invisible.btn.btn-secondary'))
        .expect(Selector('#game-edit-modal').visible).ok('Edit modal appeared', {
            timeout: 2000
        })
        .click(Selector('#Capa_1').nth(1).find('path'))
        .selectText(Selector('.new-game-name'), 0, 100)
        .typeText(Selector('.new-game-name'), 'New game name')
        .click(Selector('.game-name--edit__confirm'))
        .expect(Selector('.swal2-success').visible).ok('Game name updated successfully', {
            timeout: 3000
        })
        .wait(3000)
        .click(Selector('#Capa_1').nth(6))
        .selectTextAreaContent(Selector('.new-game-description'), 0, 0, 100, 400)
        .typeText(Selector('.new-game-description'), 'New game description', {
            caretPos: 0
        })
        .click(Selector('.game-description--edit__confirm'))
        .expect(Selector('.swal2-success').visible).ok('Game description updated successfully', {
            timeout: 3000
        })
        .setFilesToUpload('#cover-image__file', [
            '../../public/images/test-image.jpg'
        ])
        .click(Selector('#cover-image__upload'))
        .expect(Selector('.swal2-success').visible).ok('Cover image uploaded successfully', {
            timeout: 10000
        })
});

test('Edit game - Sharing & permissions', async t => {
    if (await Selector('#login-internal').exists) {
        await t
            .click(Selector('#login-internal'))
    }
    await t
        .typeText(Selector('#inputEmail'), 'eulatest1')
        .typeText(Selector('#inputPassword'), 'eulatest1')
        .click(Selector('.login--submit'))
        .click(Selector('#card-inner-template').find('.play-invisible.btn.btn-secondary'))
        .expect(Selector('#game-edit-modal').visible).ok('Edit modal appeared', {
            timeout: 2000
        })
        .click(Selector('.slider.round'))
        .expect(Selector('.swal2-success').visible).ok('Public editing permissions toggled successfully', {
            timeout: 3000
        })
        .wait(3000)
        .click(Selector('#share-link--generate'))
        .click(Selector('button').withText('Yes'))
        .expect(Selector('.swal2-success').visible).ok('Sharing link generated successfully', {
            timeout: 3000
        })
        .wait(3000)
        .click(Selector('#share-link--regenerate'))
        .click(Selector('button').withText('Yes'))
        .expect(Selector('.swal2-success').visible).ok('Link regenerated', {
            timeout: 3000
        })
        .wait(3000)
        .click(Selector('#share-link--invalidate'))
        .click(Selector('button').withText('Yes'))
        .expect(Selector('.swal2-success').visible).ok('Stopped sharing successfully', {
            timeout: 3000
        })
        .click(Selector('.view-all-analytics'))
        .expect(Selector('.game-analytics').visible).ok('Game analytics is visible', {
            timeout: 3000
        })
});
import { Selector } from 'testcafe';
import config from './config';

fixture`Play section`
    .page`${config.baseUrl}/play`;

test('Play game details', async t => {
    if (await Selector('#login-internal').exists) {
        await t
            .click(Selector('#login-internal'))
    }
    await t
        .typeText(Selector('#inputEmail'), 'eulatest1')
        .typeText(Selector('#inputPassword'), 'eulatest1')
        .click(Selector('.login--submit'))
        .wait(3000)
        .expect(Selector('.swal2-error').exists).notOk('Play page loaded with no issues', {
            timeout: 3000
        })
        .expect(Selector('#card-slider-container #card-slider-template:first-child #container .lslide.active #card-inner-template').exists).ok('Game cards exists', {
            timeout: 10000
        })
        .click(Selector('#card-slider-container #card-slider-template:first-child #container .lslide.active #card-inner-template').find('.btn-secondary'))
        .expect(Selector('#game-detail-modal').visible).ok('Game details modal appeared', {
            timeout: 2000
        })
        .click(Selector('#game-detail-modal .toggle-favourite'))
        .expect(Selector('.swal2-success').visible).ok('Game favourite state toggled successfully', {
            timeout: 3000
        })
});
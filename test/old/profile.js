import { Selector } from 'testcafe';
import config from './config';

fixture`Profile section`
    .page`${config.baseUrl}/profile`;

test('Edit profile', async t => {
    if (await Selector('#login-internal').exists) {
        await t
            .click(Selector('#login-internal'))
    }
    await t
        .typeText(Selector('#inputEmail'), 'eulatest1')
        .typeText(Selector('#inputPassword'), 'eulatest1')
        .click(Selector('.login--submit'))
        .click(Selector('.profile--edit'))
        .click(Selector('#Layer_1'))
        .click(Selector('#save-profile'))
        .expect(Selector('.swal2-success').visible).ok('Profile updated successfully', {
            timeout: 3000
        });
});